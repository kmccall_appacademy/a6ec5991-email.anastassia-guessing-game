#Anastassia Bobokalonova
#April 13, 2017

# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game

  number = 1 + rand(99)
  puts "Guess a number between 1-100!"
  user_guess = gets.chomp.to_i
  num_guesses = 1

  until user_guess == number
    puts "Your guess is #{user_guess} and it is #{user_guess > number ? "too high" : "too low"}."
    puts "Guess again!"
    user_guess = gets.chomp.to_i
    num_guesses += 1
  end

  puts "Congrats, the number was #{number}! You used #{num_guesses} guesses."

end

# guessing_game

def file_shuffler
  puts "What is the file name?"
  file_name = gets.chomp

  contents = File.readlines(file_name)
  File.write("#{file_name}-shuffled.txt", contents.shuffle)

end

# file_shuffler
